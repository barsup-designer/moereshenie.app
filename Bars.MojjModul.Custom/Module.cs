// идентификатор модуля для миграций
[assembly: Bars.B4.Modules.Ecm7.Framework.MigrationModule("Bars.MojjModul.Custom")]
// идентификатор модуля
[assembly: System.Runtime.InteropServices.Guid("818a1b8e-3119-bd20-cc45-420e2b664c19")]
namespace Bars.MojjModul.Custom
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Registrar;
    using Bars.B4.ResourceBundling;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Класс подключения модуля
    /// </summary>
    [Bars.B4.Utils.Display("Мой модуль.Разработка")]
    [Bars.B4.Utils.Description("Кастомная часть для модуля MojjModul")]
    [Bars.B4.Utils.CustomValue("Version", "2.2018.0417.2")]
    [Bars.Rms.Core.Attributes.Uid("818a1b8e-3119-bd20-cc45-420e2b664c19")]
    public partial class Module : AssemblyDefinedModule
    {
        /// <summary>
        /// Загрузка модуля
        /// </summary>
        public override void Install()
        {
            Container.RegisterResourceManifest<Bars.MojjModul.Custom.ResourceManifest>();
            RegisterControllers();
            RegisterDomainServices();
            RegisterNavigationProviders();
            RegisterPermissionMaps();
            RegisterVariables();
        }

        protected override void SetPredecessors()
        {
            base.SetPredecessors();
            SetPredecessor<Bars.MojjModul.Module>();
        }
    }
}
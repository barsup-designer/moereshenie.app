namespace Bars.MojjModul
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System;
    using Bars.B4.ResourceBundling;

    public partial class Module
    {
        protected virtual void RegisterNavigationProviders()
        {
            Component.For<Bars.B4.IClientRouteMapRegistrar>().ImplementedBy<Bars.MojjModul.ActionClientRoute>().LifestyleTransient().RegisterIn(Container);
        }
    }
}
namespace Bars.MojjModul.States
{
    using Bars.B4.Modules.States;
    using Bars.Rms.GeneratedApp.States;
    using System.Collections.Generic;

    /// <summary>
    /// Объявление статусов
    /// </summary>
    public class StatesManifest : IStatefulEntitiesManifest
    {
        /// <summary>
        /// Получить все статусные объекты
        /// </summary>
        /// <returns>Список</returns>
        public IEnumerable<StatefulEntityInfo> GetAllInfo()
        {
            var list = new List<StatefulEntityInfo>();
            return list.ToArray();
        }
    }
}
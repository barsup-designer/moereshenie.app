namespace Bars.MojjModul
{
    using Bars.B4.Utils;
    using Bars.B4;

    /// <summary>
    /// Манифест ресурсов модуля
    /// </summary>
    public partial class ResourceManifest
    {
        /// <summary>
        /// Базовая инициализация. 
        ///             Обычно вызывается из T4-шаблонов.
        /// </summary>
        /// <param name = "container"/>
        protected override void BaseInit(IResourceManifestContainer container)
        {
            AddResource(container, "content\\css\\app-icons.css");
            AddResource(container, "libs\\B4\\CustomVTypes.js");
        }

        private void AddResource(IResourceManifestContainer container, string path)
        {
            var webPath = path.Replace("\\", "/");
            var resourceName = webPath.Replace("/", ".");
            container.Add(webPath, "Bars.MojjModul.dll/Bars.MojjModul.{0}".FormatUsing(resourceName));
        }
    }
}
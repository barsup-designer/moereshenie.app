namespace Bars.MojjModul
{
    using Bars.B4.Application;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.NHibernateChangeLog;
    using Bars.B4;
    using System;
    using Bars.B4.Utils;
    using System.Linq;
    using Bars.B4;
    using Castle.Windsor;

    /// <summary>
    /// Провайдер логируемых сущностей
    /// </summary>
    public class AuditLogMapProvider : IAuditLogMapProvider
    {
        /// <summary>
        /// Инициализация
        /// </summary>
        /// <param name = "container">Контейнер реализаций логируемых сущностей</param>
        public void Init(IAuditLogMapContainer container)
        {
        }
    }
}
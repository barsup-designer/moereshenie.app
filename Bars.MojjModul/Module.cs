// идентификатор модуля для миграций
[assembly: Bars.B4.Modules.Ecm7.Framework.MigrationModule("Bars.MojjModul")]
// идентификатор модуля
[assembly: System.Runtime.InteropServices.Guid("18386ef3-b8b9-4595-97c6-d56c4f9558c1")]
namespace Bars.MojjModul
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Registrar;
    using Bars.B4.ResourceBundling;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Класс подключения модуля
    /// </summary>
    [Bars.B4.Utils.Display("Мой модуль")]
    [Bars.B4.Utils.Description("")]
    [Bars.B4.Utils.CustomValue("Version", "2.2018.0417.2")]
    [Bars.Rms.Core.Attributes.Uid("18386ef3-b8b9-4595-97c6-d56c4f9558c1")]
    public partial class Module : AssemblyDefinedModule
    {
        /// <summary>
        /// Загрузка модуля
        /// </summary>
        public override void Install()
        {
            Container.RegisterResourceManifest<Bars.MojjModul.ResourceManifest>();
            Container.RegisterSingleton<Bars.Rms.GeneratedApp.Reports.ReportContract.IReportContractProvider, ReportContractProvider>();
            Container.RegisterTransient<Bars.B4.License.ILicenseInfo, LicenceInformation>();
            RegisterControllers();
            RegisterDomainServices();
            RegisterNavigationProviders();
            RegisterPermissionMaps();
            RegisterVariables();
        }

        protected override void SetPredecessors()
        {
            base.SetPredecessors();
            SetPredecessor<Bars.B4.Icons.Module>();
            SetPredecessor<Bars.B4.Modules.Audit.Module>();
            SetPredecessor<Bars.B4.Modules.AutoMapper.Module>();
            SetPredecessor<Bars.B4.Modules.ClientOlap.Module>();
            SetPredecessor<Bars.B4.Modules.CodeMirror.Module>();
            SetPredecessor<Bars.B4.Modules.DatabaseMutex.Module>();
            SetPredecessor<Bars.B4.Modules.ECM7.Module>();
            SetPredecessor<Bars.B4.Modules.EsiaOauth.Module>();
            SetPredecessor<Bars.B4.Modules.ExtJs.Module>();
            SetPredecessor<Bars.B4.Modules.ExtJSLoaderSupport.Module>();
            SetPredecessor<Bars.B4.Modules.FIAS.Module>();
            SetPredecessor<Bars.B4.Modules.FileStorage.Module>();
            SetPredecessor<Bars.B4.Modules.Filter.Module>();
            SetPredecessor<Bars.B4.Modules.FlexDesk.Module>();
            SetPredecessor<Bars.B4.Modules.Formula.Module>();
            SetPredecessor<Bars.B4.Modules.GenericUserProfile.Module>();
            SetPredecessor<Bars.B4.Modules.Highcharts.Module>();
            SetPredecessor<Bars.B4.Modules.jQuery.Module>();
            SetPredecessor<Bars.B4.Modules.JSDeferred.Module>();
            SetPredecessor<Bars.B4.Modules.MacroAccessManagement.Module>();
            SetPredecessor<Bars.B4.Modules.Mapping.Module>();
            SetPredecessor<Bars.B4.Modules.Mapping.NHibernate.Module>();
            SetPredecessor<Bars.B4.Modules.NH.Migrations.Module>();
            SetPredecessor<Bars.B4.Modules.NH.Module>();
            SetPredecessor<Bars.B4.Modules.NH.Postgres.Module>();
            SetPredecessor<Bars.B4.Modules.NHibernateChangeLog.ExtJS4.Module>();
            SetPredecessor<Bars.B4.Modules.NHibernateChangeLog.Module>();
            SetPredecessor<Bars.B4.Modules.Nsi.ChangeRequest.Module>();
            SetPredecessor<Bars.B4.Modules.Nsi.Module>();
            SetPredecessor<Bars.B4.Modules.Nsi.QueryDesigner.Module>();
            SetPredecessor<Bars.B4.Modules.Nsi.Web.Module>();
            SetPredecessor<Bars.B4.Modules.Pivot.Module>();
            SetPredecessor<Bars.B4.Modules.PostgreSql.Module>();
            SetPredecessor<Bars.B4.Modules.Quartz.Module>();
            SetPredecessor<Bars.B4.Modules.QueryDesigner.ExtJS4.Module>();
            SetPredecessor<Bars.B4.Modules.QueryDesigner.Module>();
            SetPredecessor<Bars.B4.Modules.ReportDesigner.Module>();
            SetPredecessor<Bars.B4.Modules.ReportManager.ExtJs4.Module>();
            SetPredecessor<Bars.B4.Modules.ReportManager.Module>();
            SetPredecessor<Bars.B4.Modules.ReportPanel.Module>();
            SetPredecessor<Bars.B4.Modules.Reports.Module>();
            SetPredecessor<Bars.B4.Modules.RequireJS.Module>();
            SetPredecessor<Bars.B4.Modules.Scripting.Js.Module>();
            SetPredecessor<Bars.B4.Modules.Scripting.Module>();
            SetPredecessor<Bars.B4.Modules.Security.ExtJS4.Module>();
            SetPredecessor<Bars.B4.Modules.Security.Module>();
            SetPredecessor<Bars.B4.Modules.Security.Web.Module>();
            SetPredecessor<Bars.B4.Modules.Setup.Module>();
            SetPredecessor<Bars.B4.Modules.States.Module>();
            SetPredecessor<Bars.B4.Modules.StimulReportGenerator.Module>();
            SetPredecessor<Bars.B4.Modules.TaskManager.Contracts.Module>();
            SetPredecessor<Bars.B4.Modules.Variables.Module>();
            SetPredecessor<Bars.B4.UI.ExtJs4.Module>();
            SetPredecessor<Bars.Rms.Core.Module>();
            SetPredecessor<Bars.Rms.GeneratedApp.Module>();
        }
    }
}
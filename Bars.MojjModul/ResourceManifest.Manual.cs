namespace Bars.MojjModul
{
    using Bars.B4;
    using Bars.B4.Modules.ExtJs;
    using Bars.B4.Modules.Security;
    using Bars.B4.Utils;
    using System.Collections.Generic;
    using System.Linq;

    public partial class ResourceManifest : ResourceManifestBase
    {
        protected override void AdditionalInit(IResourceManifestContainer container)
        {
#region Перечисления
#endregion
#region Модели
            container.RegisterExtJsModel<Bars.B4.Modules.FileStorage.FileInfo>().ActionMethods(read: "POST").Controller("Empty");
#endregion
        }
    }
}
namespace Bars.MojjModul
{
    using Bars.B4.License;

    /// <summary>
    /// Информация о лицензии
    /// </summary>
    public class LicenceInformation : ILicenseInfo
    {
        public string GetLicenseInfo()
        {
            var result = string.Empty;
            if (Bars.B4.License.License.IsInit && Bars.B4.License.License.IsActive)
            {
                result = Bars.B4.License.License.GetValue("number");
            }

            return "Номер лицензии: " + result;
        }
    }
}
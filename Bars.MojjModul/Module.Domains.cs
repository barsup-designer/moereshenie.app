namespace Bars.MojjModul
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.States;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System;

    public partial class Module
    {
        protected virtual void RegisterDomainServices()
        {
            Component.For<Bars.B4.Modules.States.IStatefulEntitiesManifest>().ImplementedBy<Bars.MojjModul.States.StatesManifest>().LifestyleTransient().RegisterIn(Container);
            Component.For<Bars.B4.DataAccess.INhibernateConfigModifier>().ImplementedBy<Bars.MojjModul.NHibernateConfigurator>().LifestyleTransient().RegisterIn(Container);
            Component.For<Bars.B4.IModuleDependencies>().ImplementedBy<Bars.MojjModul.ModuleDependencies>().LifestyleSingleton().RegisterIn(Container);
            Component.For<Bars.B4.Modules.NHibernateChangeLog.IAuditLogMapProvider>().ImplementedBy<Bars.MojjModul.AuditLogMapProvider>().LifestyleSingleton().RegisterIn(Container);
            NHibernateConfigurator.RegisterAll();
        }
    }
}
namespace Bars.MojjModul
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модуль регистрации контроллеров
    /// </summary>    
    public partial class Module
    {
        /// <summary>
        /// Регистрация контроллеров
        /// </summary>
        protected virtual void RegisterControllers()
        {
        }
    }
}
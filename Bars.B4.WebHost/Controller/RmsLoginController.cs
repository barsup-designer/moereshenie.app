using System.Web.Mvc;
using Bars.B4.Modules.EsiaOauth.Controllers;

namespace Bars.B4.WebHost.Controller
{
    public class RmsLoginController : LoginEsiaOverridenController
    {
        /// <summary>Creates a <see cref = "T:System.Web.Mvc.ViewResult"/> object using the view name, master-page name, and model that renders a view.</summary>
        /// <returns>The view result.</returns>
        /// <param name = "viewName">The name of the view that is rendered to the response.</param>
        /// <param name = "masterName">The name of the master page or template to use when the view is rendered.</param>
        /// <param name = "model">The model that is rendered by the view.</param>
        protected override ViewResult View(string viewName, string masterName, object model)
        {
            if (model != null)
                ViewData.Model = model;
            ViewResult viewResult = new ViewResult{ViewName = "rmslogin", MasterName = masterName, ViewData = ViewData, TempData = TempData, ViewEngineCollection = ViewEngineCollection};
            return viewResult;
        }

        public override ActionResult Index(string login, string password, string forceAuth)
        {
            ViewData["Title"] = Container.Resolve<IApplicationInfo>().Title;
            return base.Index(login, password, forceAuth);
        }
    }
}